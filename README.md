## DeviceClient

The DeviceClient software described here is designed to run on various microcontroller devices, allowing a client to connect over WiFi to a Dynamic Control Data Protocol (DCDP) server and forward various events to it. At startup, the device joins the local wifi network and broadcasts a discovery message to locate any DCDP servers. The device attempts to connect to the first responding server. From then onward, whenever some device event (e.g. button press) is triggered in the client, a DCDP-formatted message is sent to the server. This has the effect of simulating a real keypad device (e.g. [Xkeys XK-24](https://xkeys.com/xk24.html)), seemingly attached directly to the server itself.

An implementation of a [DCDP server is available here](https://gitlab.com/chris.willing.oss/dcdp-server).


### Status

Client software for a number of microcontroller devices has been implemented, namely:

- [Nano 33 IoT](./Nano33Iot)
- [ESP32](./MqttDeviceclient)
- [ESP32-E (Firebeetle)](./firebeetle-esp32-E)

Python and Javascript versions are also available for Raspberry Pi boards:
- [Pi Zero W](./PiZeroW)



These are all at varying stages of completion but aim to eventually include the following features in common:
1. WiFi Manager for dynamic entry of local WiFi network credentials
2. capture of physical device events from keyboard, slider etc., and forward them to a DCDP server
3. sleep after some period of inactivity; wake and continue when required by some activity
4. run from battery, recharging when device is plugged in again


### Installation

Instructions to install and/or run the software is contained in each of the device directories.

### Support
Problems, comments and questions should be directed to the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.


### Contributing
Merge Requests to fix bugs are welcome via the [Merge requests](https://gitlab.com/chris.willing.oss/DeviceClient/-/merge_requests) section. Major feature additions are probably best discussed first in the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.

### License
MIT 

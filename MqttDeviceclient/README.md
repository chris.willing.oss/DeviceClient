## Mqtt DeviceClient

The Dynamic Control Data Protocol (DCDP) is a cross platform, language agnostic, message based protocol. Implementations may choose what hardware to support, as well as what network protocols clients may use to communicate. The DCDP-Server allows client communication via UDP, TCP or MQTT. The DeviceCLient software described here uses MQTT to communicate with the DCDP-Server.


### Status

This DeviceClient software was developed using a generic [ESP32 development board](https://www.amazon.com.au/Bluetooth-Development-Compatible-ESP32-DevKitC-Micropython/dp/B0C656786G/ref=sr_1_4?crid=20YBI5OTMKCBV&keywords=lonely+binary&qid=1690500349&sprefix=lonely+binary%2Caps%2C254&sr=8-4). Also under development is a version for the [ESP32-E Firebeetle 2](https://www.dfrobot.com/product-2195.html) due to its inclusion of an onboard battery recharger.

Each implementation includes all the stated goals of the project, namely:
1. WiFi Manager for dynamic entry of local WiFi network credentials
2. capture of physical device events from keyboard, slider etc., and forward them to a DCDP server
3. sleep after some period of inactivity; wake and continue when required by some activity
4. run from battery, recharging when device is plugged in again


### Installation

Installation is possible by either of two methods:
1. upload and run the provided binary
2. open the source code in Arduino-IDE to compile & upload. This mode is required if any changes are to be made e.g. using a 4x4 keypad instead of the default 4x3.


### Support
Problems, comments and questions should be directed to the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.


### Contributing
Merge Requests to fix bugs are welcome via the [Merge requests](https://gitlab.com/chris.willing.oss/DeviceClient/-/merge_requests) section. Major feature additions are probably best discussed first in the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.

### License
MIT 

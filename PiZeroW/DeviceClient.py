#!/usr/bin/env python3

import socket
import uuid
import RPi.GPIO as GPIO
import time
from random import random
import paho.mqtt.client as mqtt
import json
import asyncio
import socket
import sys

SERVICE_TARGET = "mqtt"
BROADCAST_IP = "255.255.255.255"
BROADCAST_DELAY = 3
DISCOVERY_PORT = 48895
PUB_TOPIC = "/dcdp/node"
SUB_TOPIC = "/dcdp/server/#"
VENDOR_ID = "1523"
PRODUCT_ID = "9168"
ROW_PINS = [5, 6, 13, 19]
COL_PINS = [12, 16, 20, 21]
button_down = -1
my_client_name = socket.gethostname().split('.')[0] + '-' + hex(uuid.getnode())[2:]

discover_message = json.dumps({"msg_type":"discover"});

"""
/*      An array of suitable service(s) objects keyed by
*       the host (ip address) providing it/them.
*       Some hosts may provide multiple service types.
*
*       [ {"host0":[{s0},{s1},...]}, {"host1":[{s0},{s1},...]}, ... ]
*/
"""
dcdp_services = [];



class DCDPdiscovery:

    def __init__(self, loop):
        self.loop = loop

    def connection_made(self, transport):
        self.transport = transport
        sock = transport.get_extra_info("socket")
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.broadcast()

    def datagram_received(self, data, addr):
        #print('data received:', data, addr)
        msg = json.loads(data)
        if not "msg_type" in msg:
            return
        if msg["msg_type"] == "discover_result":
            if "services" in msg:
                for service in msg["services"]:
                    if service["type"] == SERVICE_TARGET:
                        if service["ifaddress"] not in [el["ifaddress"] for el in dcdp_services]:
                            service["server_id"] = msg["server_id"]
                            dcdp_services.append(service)
                        #print("dcdp_services: ", dcdp_services)
                        #print()
            else:
                print("[INFO] No services from server at: ", msg["xk_server_address"])

    def broadcast(self):
        if len(dcdp_services) == 0:
            print("Searching for DCDP server specifying MQTT service ...")
            self.transport.sendto(discover_message.encode(), (BROADCAST_IP, DISCOVERY_PORT))
            self.loop.call_later(BROADCAST_DELAY, self.broadcast)
        else:
            self.loop.stop()


def millis():
    return int((time.time() - start_time) * 1000)

def choose_udp_port():
    udp_port = (int(random() * 100000)) % 65536
    while udp_port < 49152:
        udp_port += (65536 - 49152)
    return udp_port

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

class  DeviceClientConfig:
    def __init__(self):
        self.device = "RemoteButtonTest"
        self.client_name = socket.gethostname().split('.')[0] + '-' + hex(uuid.getnode())[2:]
        self.vendor_id = VENDOR_ID
        self.product_id = PRODUCT_ID
        self.unit_id = "0"
        self.rowCount = str(len(ROW_PINS))
        self.colCount = str(len(COL_PINS))
        self.svr_address = ""
        self.svr_port = ""
        self.caddress = get_ip()
        self.cport = str(choose_udp_port())


def send_dcdp_connect ():
    msg = {
        "msg_type": "device_connect",
        "device": config.device,
        "client_name": config.client_name,
        "vendor_id": config.vendor_id,
        "product_id": config.product_id,
        "unit_id": config.unit_id,
        "rowCount": config.rowCount,
        "colCount": config.colCount,
        "caddress": config.caddress,
        "cport": config.cport
    }
    #print("Sending {}".format(json.dumps(msg)))
    client.publish(PUB_TOPIC, json.dumps(msg))

def send_keypress(button_id, updown):
    msg = {
        "msg_type": "device_data",
        "event_type": "button_event",
        "control_id": button_id,
        "row": (button_id - 1 ) % len(ROW_PINS),
        "col": int((button_id - 1 ) / len(ROW_PINS)),
        "value": updown,
        "timestamp": millis(),
        "caddress": config.caddress,
        "cport": config.cport
    }
    #print("Sending {}".format(json.dumps(msg)))
    client.publish(PUB_TOPIC, json.dumps(msg))

def event_callback(pin):
    global button_down
    if button_down > -1:
        send_keypress(button_down, 0)
        button_down = -1
        return

    # We don't use this
    #value = GPIO.input(pin)

    active_column = COL_PINS.index(pin)
    for row in range(len(ROW_PINS)):
        GPIO.output(ROW_PINS[row], GPIO.HIGH)
        if (GPIO.input(COL_PINS[active_column]) == GPIO.HIGH):
            control_id = len(ROW_PINS)*active_column + row
            send_keypress(control_id, 1)
            button_down = control_id
            GPIO.output(ROW_PINS[row], GPIO.LOW)
            return
        GPIO.output(ROW_PINS[row], GPIO.LOW)

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print(f"Connected successfully to MQTT broker with result code {rc}")
        client.subscribe(SUB_TOPIC)
    else:
        print(f"MQTT connect failure with code {rc}")

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscription looks OK - mid = {}".format(mid))
    send_dcdp_connect()

def on_publish(client, userdata, mid):
    #print("Message published with mid: {}".format(mid)) 
    pass

def on_message(client, userdata, message):
    print("Received message: {}".format(str(message.payload.decode("utf-8"))))
    msg = json.loads(str(message.payload.decode("utf-8")))
    if msg['msg_type'] == "device_connect_result":
        pass
    elif msg['msg_type'] == "device_disconnect_result":
        pass
    elif msg['msg_type'] == "disconnect_warning":
        # Check that this is for us
        if msg['client_port'] == config.cport:
            send_dcdp_connect()

if __name__ == '__main__':
    start_time = time.time()
    print("Starting up")
    
    loop = asyncio.get_event_loop()
    coro = loop.create_datagram_endpoint(
        lambda: DCDPdiscovery(loop), local_addr=('0.0.0.0', 0))
    loop.run_until_complete(coro)
    loop.run_forever()
    loop.close()

    if len(dcdp_services) == 0:
        sys.exit()

    # We choose the first available service (dcdp_services[0])
    #
    MQTT_SVR = dcdp_services[0]["ifaddress"]
    MQTT_PORT = dcdp_services[0]["port"]
    print("Trying MQTT server at ", MQTT_SVR)

    config = DeviceClientConfig()

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    for row_pin in ROW_PINS:
        GPIO.setup(row_pin, GPIO.OUT)
        GPIO.output(row_pin, GPIO.LOW)

    for col_pin in COL_PINS:
        GPIO.setup(col_pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
        GPIO.add_event_detect(col_pin, GPIO.BOTH,
                          callback=event_callback,
                          bouncetime=120)


    client = mqtt.Client()
    client.on_connect = on_connect 
    client.on_subscribe = on_subscribe 
    client.on_publish = on_publish 
    client.on_message = on_message 
    client.connect(MQTT_SVR, MQTT_PORT, 60) 
    client.loop_forever()


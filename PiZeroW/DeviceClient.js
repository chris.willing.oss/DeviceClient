#!/usr/bin/env node

const os = require('os');
const Gpio = require('onoff').Gpio;
const mqtt = require('mqtt');

const service_target = "mqtt";
let client;
//const MQTT_URL = "mqtt://broker.emqx.io";
//const MQTT_URL = "mqtt://test.mosquitto.org";
//const MQTT_URL = "mqtt://192.168.20.28";
const PUB_TOPIC = "/dcdp/node";
const SUB_TOPIC = "/dcdp/server/#";
const QOS = 2;
const ROW_PINS = [5, 6, 13, 19];
const COL_PINS = [12, 16, 20, 21];

const row_pinobjs = [];
const col_pinobjs = [];
console.log(`ROW pins: ${ROW_PINS}`);
console.log(`COL pins: ${COL_PINS}`);

// Start button numbering at 0 or 1?
const BUTTON_BASE = 1;

// ID of button currently pressed, where -1 means nothing is pressed
var button_down = -1;


class Configuration {
	constructor () {
		this.device = "RemoteButtonTest";
		this.client_name = make_client_name();
		this.vendor_id = "1523";
		this.product_id = "9168";
		this.unit_id = "0";
		this.rowCount = ROW_PINS.length.toString();
		this.colCount = COL_PINS.length.toString();
		this.svr_address = "";
		this.svr_port = "";
		this.caddress = get_ip();
		// This is a reasonable backup for cport.
		// Replace it with port number used for discover message
		this.cport = choose_udp_port().toString();
	}
}


/*      Using UDP for discovery of mqtt message service
*/
const discovery_port = 48895;
var dgram = require("dgram");
var socket = dgram.createSocket("udp4");
socket.bind( () => {
        socket.setBroadcast(true);
});
const discover_message = JSON.stringify({"msg_type":"discover"});

/*      An array of suitable service(s) objects keyed by
*       the host (ip address) providing it/them.
*       Some hosts may provide multiple service types.
*
*       [ {"host0":[{s0},{s1},...]}, {"host1":[{s0},{s1},...]}, ... ]
*/
var dcdp_services = [];

socket.on('listening', () => {
	// Replace config.cport placeholder
	//const address = socket.address();
	//console.log(`socket listening ${address.address}:${address.port}`);
	config.cport = socket.address().port;
	console.log(`Using cport = ${config.cport}`);
});

socket.on("message", (message, rinfo) => {
        const msg = JSON.parse(message);
	console.log(`Address: ${JSON.stringify(socket.address())}`);
	if (msg.msg_type == "discover_result") {	
		if (Object.keys(msg).includes("services")) {
			msg.services.forEach( (service) => {
				console.log(`service: ${JSON.stringify(service)}`);
				if (service.type == service_target) {
					console.log(`considering service from ${msg.server_id}: ${JSON.stringify(service)}`);
					// Ignore if we have already seen this service from this server
					if (dcdp_services.find(entry => { return entry.ifaddress === service.ifaddress ; }) ) {
						console.log(`Not adding duplicate address ${service.ifaddress}`);
					} else {
						var service_entry = service;
						service_entry.server_id = msg.server_id;
						dcdp_services.push(service_entry);
					}
				}
			});
		} else {
			// Must be an older server which doesn't provide a "services" field
			console.log(`Response from ${msg.xk_server_address} has no 'services' field. Please update the server.`);
		}
	} else {
		// Not interested in anything else
		console.log(`Unknown message type ${msg.msg_type}`);
	}

});


/*  choose_server(interval)
*
*   interval: milliseconds between retries.
*
*   Send discovery messages until a reply is received
*/
function choose_server (interval = 2000) {
	if (dcdp_services.length == 0) {	
		/*      No servers found so try again.
		*/
		console.log(`Finding DCDP servers ...`);
		socket.send(discover_message, 0, discover_message.length, discovery_port, '255.255.255.255', function(err, bytes) { });
		setTimeout(choose_server, interval);
		return;
	} else {
		mqtt_server = dcdp_services[0];
	}
	console.log(`Discovered DCDP server with ${service_target} service: ${mqtt_server.server_id} at ${mqtt_server.ifaddress}:${mqtt_server.port}`);
	client = mqtt.connect("mqtt://" + mqtt_server.ifaddress, mqtt_server.port);

	client.on('connect', (err) => {
		console.log(`connected to MQTT server`);	
		socket.close()
		client.subscribe(SUB_TOPIC, function (err) {
			client.publish(PUB_TOPIC, JSON.stringify({msg_type:"hello",data:"Hello from new DeviceCLient"}),{qos:QOS,retain:false});
			if (!err) {
				console.log(`dcdp-server subscribed to MQTT server OK`);
				send_dcdp_connect();
			} else {
				// Any point in going on?
				console.log('Exiting due to MQTT subscription failure: ' + err);
				process.exit(1);
			}
		});
	});

	client.on('message', (topic, message) => {
		console.log(`Received message from topic: ${topic}, message: ${message}`);	
		try {
			var msg = JSON.parse(message);
			if (msg['msg_type'] == "disconnect_warning") {
				// Check that this is for us
				if (msg['client_port'] == config.cport) {
					send_dcdp_connect();
				}
			}
			else if (msg['msg_type'] == "device_connect_result") {
				// If this is for us, make use of port number
				if (msg['client_name'] == config.client_name) {
					console.log(`Device connected as ${msg['client_name']}`);
				}
			}
			else if (msg['msg_type'] == "device_disconnect_result") {
				console.log(`Device disconnected`);
			}
		}
		catch (err) {
			console.error(`ERROR: couldn't parse incoming message ${message}`);
		}
	});
}


/* main() */
const start_time = Date.now();

const config = new Configuration();
console.log(`Using client_name: ${config.client_name}`);
console.log(`Using caddress: ${config.caddress}`);

//process.exit();


for (pin of ROW_PINS) {
	rowobj = {};
	rowobj['pin_id'] = pin;
	rowobj['pushbutton'] = new Gpio(pin, 'out');
	rowobj['pushbutton'].writeSync(Gpio.LOW);
	row_pinobjs.push(rowobj);

}

for (pin of COL_PINS) {
	pinobj = {};
	pinobj['pin_id'] = pin;
	pinobj['pushbutton'] = new Gpio(pin, 'in', 'both');
	pinobj['pushbutton'].watch(function (err, value) {
		if (err) {
			console.error('There was an error', err);
			return;
		}
		//console.log(`Pin ${this.pin_id} ${value==0?"DOWN":"UP"} (${value})`);
		if ((button_down > -1) || (value == 1)) {
			if (value == 1) {
				if (button_down > -1) {
					// Do something with this button (button_down)
					//console.log(`control_id ${button_down} UP`);
					send_keypress(control_id, 0);

					button_down = -1;
				}
			}
			return;
		}
		active_column = COL_PINS.indexOf(this.pin_id);
		if (active_column < 0) return;
		for (row in ROW_PINS) {
			rowobj = row_pinobjs[row];
			rowobj['pushbutton'].writeSync(Gpio.HIGH);
			//if (GPIO.input(COL_PINS[active_column]) == GPIO.HIGH):
			if (col_pinobjs[active_column]['pushbutton'].readSync() == Gpio.HIGH) {
				control_id = parseInt(active_column)*parseInt(row_pinobjs.length) + parseInt(row);

				// Do something with this button (control_id)
				//console.log(`control_id ${control_id} DOWN`);
				send_keypress(control_id, 1);
				button_down = control_id
			}
			rowobj['pushbutton'].writeSync(Gpio.LOW);
		}
	}.bind(pinobj));	
	col_pinobjs.push(pinobj);
}


function unexportOnClose() {
	for (obj of col_pinobjs) {
		obj.pushbutton.unexport();
	}
}


/* Generate a client name from hostname & mac address
   TODO:
	 toughen up assumption that we have a 'wlan0' interface
*/
function make_client_name() {
	const hostname_part = os.hostname().split('.')[0];
	//console.log(`hostname = ${hostname_part}`);

	const mac_id_part = os.networkInterfaces()['wlan0'][0].mac.replaceAll(':','');
	//console.log(`mac addr = ${mac_id_part}`);

	return hostname_part + '-' + mac_id_part;
}

function millis() {
	return Date.now() - start_time;
}

function choose_udp_port() {
	//var udp_port = (Math.floor(Math.random() * 100000)) % 65536;
	var udp_port = Math.floor(Math.random(65536) * 65536);
	while (udp_port < 49152) {
		udp_port += (65536 - 49152);
	}
	return udp_port;
}

/* Return IP adddress
   TODO:
	 toughen up assumption that we have a 'wlan0' interface
*/
function get_ip () {
	return os.networkInterfaces()['wlan0'][0].address;
}

function send_dcdp_connect () {
    //console.log(`sending DCDP connect message`);
    msg = {
        "msg_type": "device_connect",
        "device": config.device,
        "client_name": config.client_name,
        "vendor_id": config.vendor_id,
        "product_id": config.product_id,
        "unit_id": config.unit_id,
        "rowCount": config.rowCount,
        "colCount": config.colCount,
        "caddress": config.caddress,
        "cport": config.cport
    }
    //console.log(`Sending ${JSON.stringify(msg)}`)
    client.publish(PUB_TOPIC, JSON.stringify(msg))
}

function send_keypress(button_id, updown) {
    //console.log(`sending button_id: ${button_id}`);
    msg = {
        "msg_type": "device_data",
        "event_type": "button_event",
        "control_id": button_id + BUTTON_BASE,
        "row": (button_id + BUTTON_BASE - 1 ) % ROW_PINS.length,
        "col": parseInt((button_id - 1 ) / ROW_PINS.length),
        "value": updown,
        "timestamp": millis(),
        "caddress": config.caddress,
        "cport": config.cport
    }
    //console.log(`Sending ${JSON.stringify(msg)}`);
    client.publish(PUB_TOPIC, JSON.stringify(msg))
}



choose_server();


## Raspberry Pi boards

The form factor of Raspberry Pi Zero boards is similar to the ESP32 based MCU boards, making them a possible contender for implementing a DeviceClient. It may not be so attractive for battery based operation but would be useful in situations where connection to a power supply is available.

Python and Javascript versions of a DeviceClient are in development and are available here. Both versions use MQTT to exchange messages with the DCDP server. The Python version is currently able to send keypad events via a preconfigured MQTT server. The Javascript version implements the DCDP discovery protocol, so is able to discover the MQTT server to send events to.

### Setup

The Python version needs the _RPi.GPIO_ and _paho-mqtt_ modules to be installed. Install them with:
```
    sudo apt install python3-paho-mqtt python3-rpi.gpio
```

For the Javascript version, the _onoff_ and _mqtt_ modules are required. Install them with:
```
    npm install onoff mqtt
```
Any available pins may be used to attach the keypad to the pi. The actual pins used should be entered into the code's ROW_PINS and COL_PINS arrays. The built in values are for a 4x4 keypad. A 4x3 keypad would have only three pin numbers in the COL_PINS array. The code is designed to tolerate keypad matrices of any dimension although, obviously, the dimensions will ultimately be restricted by the number of available GPIO pins on the pi.

### Support

Problems, comments and questions should be directed to the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.

### License
MIT

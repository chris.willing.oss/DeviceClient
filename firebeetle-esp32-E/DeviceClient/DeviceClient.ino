#include <Key.h>
#include <Keypad.h>

#include <ESP32TimerInterrupt.h>
#include <ESP32TimerInterrupt.hpp>

#include <ArduinoJson.h>
#include <ArduinoJson.hpp>

#include "WiFi.h"

#include "arduino_secrets.h"

const char *ssid = SECRET_SSID;
const char *password = SECRET_PASS;

const byte ROWS = 4;
const byte COLS = 3;
// define the symbols on the buttons of the keypad
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {D10, D11, D12, D13}; // connect to the row pinouts of the keypad
//byte rowPins[ROWS] = {D10, 16, 4, 12}; // connect to the row pinouts of the keypad
byte colPins[COLS] = {D2, D3, D6};         // connect to the column pinouts of the keypad

// initialize an instance of class NewKeypad
Keypad myKeypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);


const int discovery_port = 48895;
WiFiUDP Udp;

struct ConfigData {
  char device[32];
  char vendor_id[16];
  char product_id[16];
  char unit_id[16];
  char rowCount[8];
  char colCount[8];
  char svr_address[16];
  char svr_port[8];
  char client_name[32];
};
ConfigData *config = (ConfigData*)std::calloc(sizeof(ConfigData), 1);

// Have we been connected to a DCDP server?
// Yes => device_connect_result has already been received
RTC_DATA_ATTR int DCDP_connected = 0;
RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR int local_udp_port;
RTC_DATA_ATTR char rtc_svr_address[16];
RTC_DATA_ATTR char rtc_svr_port[8];
RTC_DATA_ATTR char rtc_client_name[32];

// request timestamp in ms:
long lastRequest = 0;
// interval between requests:
int discover_interval = 3000;

// Calculate mask for multiple bins e.g. pins 15 and 2, from bash: printf "%x\n" $(echo "2^15 + 2^2" |bc)
//#define BUTTON_PIN_BITMASK 0x2000000      // 2^25 in hex - from bash: printf "%x\n" $(echo "2^25" |bc)
#define BUTTON_PIN_BITMASK 0x2000      // 2^13 in hex - from bash: printf "%x\n" $(echo "2^13" |bc)

// Init ESP32 timer 0 as sleep trigger
#define TIMER0_INTERVAL_MS        1000
#define PIN_LED 2
ESP32Timer ITimer0(1);
volatile uint32_t Timer0Count = 0;

bool IRAM_ATTR TimerHandler0 (void * timerNo) {
  static bool toggle0 = false;

  toggle0 = !toggle0;
  digitalWrite(PIN_LED, toggle0);

  if(++Timer0Count > 59) {
    //esp_wifi_stop();
    esp_deep_sleep_start();
  }

  return true;
}

void RTC_IRAM_ATTR esp_wake_deep_sleep(void) {
   esp_default_wake_deep_sleep();
    // Add additional functionality here
    if (bootCount < 20) {
      bootCount += 5;
    }

  // Reset the sleep timer
  Timer0Count = 0;
}

/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}

void setup()
{
  Serial.begin(115200);
  delay(500);

  pinMode(PIN_LED, OUTPUT);

  //Print the wakeup reason for ESP32
  print_wakeup_reason();

  // Wake from sleep by external set on pin 13
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_13, 1); //1 = High, 0 = Low

  // Interval in microsecs
  //  1000 * 1000 =  1 second
  // 60000 * 1000 = 60 seconds
	if (ITimer0.attachInterruptInterval(TIMER0_INTERVAL_MS * 1000, TimerHandler0)) {
		Serial.print(F("Starting  ITimer0 OK, millis() = "));
		Serial.println(millis());
	}
	else {
		Serial.println(F("Can't set ITimer0. Select another Timer, freq. or timer"));
  }

  myKeypad.addEventListener(keypadEvent); // Add an event listener for this keypad
  myKeypad.setHoldTime(10);              // Default is 1000mS

  // Device configuration
  strlcpy(config->device, "RemoteButtonTest", String("RemoteButtonTest").length()+1);
  itoa(1523, config->vendor_id, 10);
  itoa(9167, config->product_id, 10);
  itoa(0, config->unit_id, 10);
  itoa(4, config->rowCount, 10);
  itoa(4, config->colCount, 10);
  config->svr_address[0] = '\0';
  config->svr_port[0] = '\0';

  // Check whether new start or restart
  Serial.println("DCDP connection status = " + String(DCDP_connected));
  if (DCDP_connected == 0) {
    // Absolutely new start

    int random_suffix = random(micros());
    while (random_suffix < 100000) { random_suffix += 12345; }
    String astring = String(config->device) + "_" + String(random_suffix%100000, 16);
    astring.toCharArray(config->client_name, astring.length()+1);
    Serial.println("Choosing own client_name: " + String(config->client_name));
    strcpy(rtc_client_name, config->client_name);

    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.println("WiFi Failed");
      while(1) {
        delay(1000);
      }
    }

    local_udp_port = choose_udp_port();
    Udp.begin(local_udp_port);

    // Find a server
    discover_loop();
  }
  else {
    // Presume this is a restart after wakeup from sleep
    Serial.println("Restart after sleep");

    strcpy(config->svr_address, rtc_svr_address);
    strcpy(config->svr_port, rtc_svr_port);
    strcpy(config->client_name, rtc_client_name);

    Serial.println("client_name: " + String(config->client_name));
    Serial.println("svr_address: " + String(config->svr_address));
    Serial.println("svr_port: " + String(config->svr_port));

    //esp_wifi_start();
    WiFi.begin(ssid, password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.println("WiFi Failed");
      while(1) {
        delay(1000);
      }
    }
    else {
      Serial.println("WiFi succeeded");
      Udp.begin(local_udp_port);

      device_connect();
    }
  }
}

void loop()
{
  // Get the character input
  char keyPressed = myKeypad.getKey();
  //if (keyPressed) {
  //  Serial.println(keyPressed);
  //}
}

void
discover_loop () {
  while (String(config->svr_address).length() < 1) {
    if (millis() - lastRequest < discover_interval) {
      continue;
    }
    Serial.println("discover_loop()");

    char output[64];
    DynamicJsonDocument discoverDoc(32);
    discoverDoc["msg_type"] = "discover";

    Udp.beginPacket(IPAddress(255,255,255,255), discovery_port);
    serializeJson(discoverDoc, Udp);
    Udp.endPacket();  // finish and send discovery message

    process_network();

    lastRequest = millis();
    Timer0Count = 0;
  }
}

void
process_network() {
  //Serial.println("process_network() ...");
  if (Udp.parsePacket() > 0) {
    String message = "";
    while (Udp.available() > 0) {
      // parse the body of the message
      message = Udp.readString();
    }

    DynamicJsonDocument msg(2*message.length());
    DeserializationError error = deserializeJson(msg, message);
    if (error) {
      Serial.print("deserializeJson() failed: ");
      Serial.println(error.c_str());
    }
    String msg_type = msg["msg_type"].as<const char*>();
    //Serial.println("Received msg_type: " + msg_type);
    if (msg_type == "discover_result") {
      /*  The server advertises an array of available services.
      *   Each service is described by an object containing
      *   fields of port, ifaddress and type
      */
      JsonArray services = msg["services"].as<JsonArray>();
      for (JsonVariant value : services) {
        JsonObject  service = value.as<JsonObject>();
        //  We're only interested in UDP service (TCP or MQTT may also be available)
        if (service["type"] == "udp") {
          // We may have several options to choose from.
          // Just take the first available & ignore others
          if (strlen(config->svr_port) > 0) {
            //Serial.println("Already have a port");
            //  i.e. ignore it
          } else {
            //Serial.println("no port yet");
            // Add address & port to configuration
            itoa(service["port"].as<int>(), config->svr_port, 10);
            strlcpy(config->svr_address, service["ifaddress"].as<const char*>(), String(service["ifaddress"].as<const char*>()).length()+1);

            // Also add them to rtc memory (for wakeup from sleep)
            strcpy(rtc_svr_address, config->svr_address);
            strcpy(rtc_svr_port, config->svr_port);

            Serial.println("remote port = " + String(config->svr_port));

            device_connect();
          }
        }
      }
    }
    else if (msg_type == "device_connect_result") {
      Serial.println("Device connect was successful");
      DCDP_connected = 1;
    }
    else if ((msg["client_name"] == config->client_name) && (msg_type == "disconnect_warning")) {
      Serial.println("Device disconnect warning");
    }
    else if ((msg["client_name"] == config->client_name) && (msg_type == "device_disconnect_result")) {
      Serial.println("We've been disconnected!");
    }
    else if (msg_type == "button_event") {
      // Ignore
    }
    else {
      Serial.println("Unhandled msg_type: " + msg_type);
    }
  }
}

long
choose_udp_port () {
  long udp_port = random(micros()) % 65536;
  while (udp_port < 49152) {
    udp_port += (65536-49152);
  }
  // Check whether already used?
  return udp_port;
}

void
device_connect() {
    IPAddress ipaddr;
    Serial.println("device_connect to DCDP server: " + String(config->svr_address));
    if (ipaddr.fromString(config->svr_address)) {
      int connectDocCapacity = JSON_OBJECT_SIZE(8);
      //Serial.println("connectDocCapacity: " + String(connectDocCapacity));
      DynamicJsonDocument connectDoc(2*connectDocCapacity);
      connectDoc["msg_type"] = "device_connect";
      connectDoc["device"] = config->device;
      connectDoc["vendor_id"] = config->vendor_id;
      connectDoc["product_id"] = config->product_id;
      connectDoc["unit_id"] = config->unit_id;
      connectDoc["rowCount"] = config->rowCount;
      connectDoc["colCount"] = config->colCount;
      if (String(config->client_name).length() > 0) {
        connectDoc["client_name"] = config->client_name;
      }
      
      Udp.beginPacket(ipaddr, String(config->svr_port).toInt());
      if (serializeJson(connectDoc, Udp) == 0) {
        Serial.println("Couldn't serialize device_connect message");
      }
      else {
        Udp.endPacket();     // finish and send device_connect message

        DCDP_connected = 0;
        while (DCDP_connected == 0) {
          process_network();
        }
      }
    }
}

void
send_keypress (int buttonID, int updown) {
  IPAddress ipaddr;
  if (ipaddr.fromString(config->svr_address)) {
    int keypressDocCapacity = JSON_OBJECT_SIZE(7);
    //Serial.println("keypressDocCapacity: " + String(keypressDocCapacity));
    //DynamicJsonDocument keypressDoc(1024);
    DynamicJsonDocument keypressDoc(keypressDocCapacity);
    keypressDoc["msg_type"] = "device_data";
    keypressDoc["event_type"] = "button_event";
    keypressDoc["control_id"] = buttonID;
    keypressDoc["row"] = (buttonID-1)%ROWS;
    keypressDoc["col"] = (buttonID-1)/ROWS;
    keypressDoc["value"] = updown;
    keypressDoc["timestamp"] = millis();

    Udp.beginPacket(ipaddr, String(config->svr_port).toInt());
    if (serializeJson(keypressDoc, Udp) == 0) {
      Serial.println("Couldn't serialize device_connect message");
    } else {
      Udp.endPacket();     // finish and send device_connect message
    }
  }
  Timer0Count = 0;

}
// Taking care of some special events.
void keypadEvent(KeypadEvent key){
  switch (myKeypad.getState()){
    case PRESSED:
      send_keypress(index_by_character(key), 1);
      break;
    case RELEASED:
      send_keypress(index_by_character(key), 0);
      break;
    case HOLD:
      //Serial.println("key HOLD");
      break;
  }
}

int
index_by_character (char c) {
  int result;
  
  switch (c) {
    case '1':
      result = 1;
      break;
    case '4':
      result = 2;
      break;
    case '7':
      result = 3;
      break;
    case '*':
      result = 4;
      break;
    case '2':
      result = 5;
      break;
    case '5':
      result = 6;
      break;
    case '8':
      result = 7;
      break;
    case '0':
      result = 8;
      break;
    case '3':
      result = 9;
      break;
    case '6':
      result = 10;
      break;
    case '9':
      result = 11;
      break;
    case '#':
      result = 12;
      break;
    case 'A':
      result = 13;
      break;
    case 'B':
      result = 14;
      break;
    case 'C':
      result = 15;
      break;
    case 'D':
      result = 16;
      break;
    default:
      result = 0;
      break;
  }
  return result;
}

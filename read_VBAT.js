#!/usr/bin/env node

/*	read_VBAT.js

	No args supplied => list log files in reports directory
	Arg supplied (log file name) => display the file
*/

const path = require('path');
const fs = require('node:fs');
const readline = require('node:readline');

const reports_directory = '/home/chris/.local/share/dcdp-server/reports' ;

function list_reports (dir) {
	files = fs.readdirSync(dir, {withFileTypes: true})
		.filter(item => item.name.endsWith('.log'))
		.map(item => item.name)
	files.forEach( (item) => console.log(item));
}

async function processLineByLine(log_file) {
  const fileStream = fs.createReadStream(path.join(reports_directory, log_file));

  const rl = readline.createInterface({
	input: fileStream,
	crlfDelay: Infinity,
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  for await (const line of rl) {
	// Each line in input.txt will be successively available here as `line`.
	//console.log(`Line from file: ${line}`);
	const entry = JSON.parse(line);
	const d = new Date(entry.timestamp_server);
	console.log(`${d.toString()} ${entry.client_name} ${entry.value}`);
  }
}

arg_count = process.argv.slice(2).length;
//console.log(`arg count = ${arg_count}`);
if (arg_count == 0 ) {
	list_reports(reports_directory);
} else {
	//console.log(`typeof = ${typeof(process.argv[2])}`);
	processLineByLine(process.argv[2]); 
}

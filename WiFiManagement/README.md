## DeviceClient WiFi Manager

To avoid hard coding credentials for individual wifi networks, we aim to include a Wifi Manager in each of the DeviceClients described in this project.

Those DeviceClients without a Wifi Manager will need to supply an SSID (network name) and password somehow to the DevciceClient. Typically this is done by creating a file named arduino_secrets.h containing definintions for SECRET_SSID and SECRET_PASS. The arduino_secrets.h file is then included in the relevant .ino file which is then compiled and uploaded to the device to be used.

While this is OK during development, it is clearly inconvenient for an end user. Instead, we aim to provide a **Wifi Manager** for each DeviceClient.

### Wifi Manager Usage

When the DeviceClient starts up it attempts to join the local network. If the device was previously run on this network and correct credentials provided at that time, joining the wifi network will automatically succeed and no further user input is required i.e. the following procedure should need to be performed just once.

However if the DeviceClient fails to join the local wifi network e.g. on first use when no network credentials are known, the DeviceClient will create its own temporary wifi Access Point (AP). We can connect to this AP using a phone or computer to provide the necessary wifi credentials, which will be remembered for the next time the DeviceClient is run.

Using a phone, the temporary AP created by the DeviceCLient should be visible in the phones wifi connection settings.

<div align="center">
<img src="../pix/wfm_00_available.jpg"  width="360" height="772">
</div>

In this case, the phone is currently connected to the _deco22_ network. Amongst the other available networks is found the DeviceClient's temporary AP - shown here as _esp32-7c6b9b9ef0c8_ (the device type _esp32_ followed by the device MAC address _7c6b9b9ef0c8_).  We first need to join the phone to this network by selecting the _esp32-7c6b9b9ef0c8_ entry and providing a password for the temporary AP (currently preset to "pwdpwdpwd"), after which the phone's wifi connection setting should show the new wifi connection to the temporary AP.

Note that the Current Network entry also shows `Checking the quality of your Internet connection...`. Since the device's AP has no connection to the internet, that checking should fail and will display something like:

<div align="center">
<img src="../pix/wfm_03_nointernet.jpg"  width="360" height="772">
</div>

Select _Connect only this time_ or equivalent - other phones may display slightly different messages.

Once connected to the DeviceClient's temorary AP, close the phone's wifi connections settings and open a web browser; browse to _192.168.4.1_ which the DeviceClient has set up to accept the credentials it requires to join the wifi network.

<div align="center">
<img src="../pix/wfm_10_emptycreds_cropped.jpg"  width="360" height="772">
</div>

Enter the name and password for the wifi network that the DeviceClient should join (in this example, probably the _deco22_ network).

<div align="center">
<img src="../pix/wfm_11_credsOK.jpg"  width="360" height="772">
</div>

When the credentials have been accepted, close the AP browser window. The DeviceClient should now have joined the local wifi network and started its normal operation. It will have also shut down its temporary wifi access point and the phone being used to enter credentials will probably have rejoined its usual wifi network.

### Changing credentials

If the wifi network credentials need to be changed for some reason, it can be achieved through the DeviceClient's Multi-Reset Detector (MRD) facility. Invoke this by restarting the device and pressing the device's _Reset_ button three times, each press separated by about five seconds. This will run the Wifi Manager again so that the above procedure (joining AP with phone and entering new credentials in browser) can be run again.


### Support
Problems, comments and questions should be directed to the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.


### Contributing
Merge Requests to fix bugs are welcome via the [Merge requests](https://gitlab.com/chris.willing.oss/DeviceClient/-/merge_requests) section. Major feature additions are probably best discussed first in the [Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) section.

### License
MIT 

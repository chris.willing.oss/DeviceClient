#!/bin/bash

sudo sed -i "/deviceConfig/d" /etc/rc.local
sudo sed -i "/^exit 0/i python3 $(pwd)/deviceConfig.py &" /etc/rc.local

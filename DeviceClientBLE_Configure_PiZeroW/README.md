## BLE Configuration for Raspberry Pi Zero W

This sub project implements a BLE peripheral role application to run on Raspberry Pi Zero W, enabling remote configuration of wireless SSID and password as well as monitoring of some run time characteristics (cpu, memory, uptime).


### Prerequisites

- Raspberry Pi Zero W (or similar) running RaspiOS at least 2023-10-10 (bookworm)
- python3-dbus (`sudo apt install python3-dbus`)
- python3-pip (`sudo apt install python3-pip`)
- python-ble-peripheral library (`sudo python3 -m pip install --break-system-packages pythonBLEperipheral`)


### Installation

- [Download the source tarball](https://gitlab.com/chris.willing.oss/DeviceClient/-/releases) to the device and unpack it (`tar xf DeviceClientBLE_Configure_PiZeroW-0.0.3.tar.gz`)
- Enter the resulting _DeviceClientBLE\_Configure\_PiZeroW_ directory
- Run the installation script `sudo bash install_deviceConfig.sh`. It will edit the _/etc/rc.local_ file to start up the peripheral app at boot time.
- Reboot


### Usage

When the device has rebooted it will make itself accessible via BLE. A suitable client program is required to communicate with the Pi Zero W device over BLE. [A suitable client device is available here](https://gitlab.com/chris.willing.oss/deviceclientconfig).

![](pix/smConfigClient.png)

Whether served from a web server or run from a file in a browser (as in the pix above), the browser must be Bluetooth capable e.g. Google's Chrome browser.

In the left _Device Information_ panel, various facets of the peripheral device are regularly updated.

Of the command buttons in the right _Device Commands_ panel, the purpose of the _REBOOT device_ button is obvious. The _Stop BLE_ button shuts down the configuration application (no further BLE communication is then possible). The _Locate device_ button toggles flashing of the device's onboard LED. This is useful for identifying which of possibly many devices is currently connected.

The middle _Wifi Setting_ panel enables a wifi SSID and password to be sent to the device. This is most useful if the configuration software is able to be included in the boot image initially burnt into the Pi Zero's SD. In this case, individual wifi access can be facilitated for multiple headless systems all using the same boot image.


### Support

Any problems should be addressed to the [DeviceClient Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) page.


### Contributing

Contributions are welcome either as [Merge Requests](https://gitlab.com/chris.willing.oss/DeviceClient/-/merge_requests) or comments to the [DeviceClient Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) page.


### License
MIT (http://opensource.org/licenses/MIT)

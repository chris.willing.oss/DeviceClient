#!/usr/bin/env python3

import dbus
import json
import os, re
import socket, uuid

from uuids import *
from pythonBLEperipheral.advertisement import Advertisement
from pythonBLEperipheral.service import Application, Service, Characteristic, Descriptor
from gpiozero import LoadAverage, CPUTemperature

# fullHostName
# Try to generate a name based on bluetooth hardware address, otherwise make one up
try:
    from bluetooth import read_local_bdaddr
    BLE_DEVICE_NAME = socket.gethostname().split('.')[0] + '-' + read_local_bdaddr()[0].replace(':',"").lower()
except:
    BLE_DEVICE_NAME = socket.gethostname().split('.')[0] + '-' + hex(uuid.getnode())[2:]

LOAD_AVG_1  = 0
LOAD_AVG_5  = 1
LOAD_AVG_15 = 2

GATT_CHRC_IFACE = "org.bluez.GattCharacteristic1"
NOTIFY_TIMEOUT = 5000


class ConfiguratorAdvertisement(Advertisement):
    def __init__(self, index):
        Advertisement.__init__(self, index, "peripheral")
        self.add_local_name(BLE_DEVICE_NAME)
        self.discoverable = True
        self.include_tx_power = True
        self.add_service_uuid(uuids["SERVICE_ID"])

class BLEService(Service):
    def __init__(self, index):
        Service.__init__(self, index, uuids["CONFIG_SERVICE"], True)
        self.add_characteristic(DEVICE_INFO_Characteristic(self))
        self.add_characteristic(WIFI_CREDS_Characteristic(self))
        self.add_characteristic(CMD_LOCATE_Characteristic(self))
        self.add_characteristic(CMD_REBOOT_Characteristic(self))
        self.add_characteristic(CMD_RUNBLE_Characteristic(self))
        self.add_characteristic(INFO_DEVICE_ID_Characteristic(self))
        self.add_characteristic(INFO_MODEL_Characteristic(self))
        self.add_characteristic(INFO_WIFI_AP_Characteristic(self))
        self.add_characteristic(INFO_WIFI_IP_Characteristic(self))
        self.add_characteristic(INFO_CPU_TEMP_Characteristic(self))
        self.add_characteristic(INFO_CPU_LOAD_Characteristic(self))
        self.add_characteristic(INFO_CPU_USAGE_Characteristic(self))
        self.add_characteristic(INFO_MEMORY_Characteristic(self))
        self.add_characteristic(INFO_UPTIME_Characteristic(self))
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Only add battery info if we can connect
        try:
            sock.connect(('localhost', 8423))
        except:
            pass
        else:
            self.add_characteristic(INFO_BATTERY_Characteristic(self))
        finally:
            sock.close()


class DEVICE_INFO_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        Characteristic.__init__(
                self, uuids["DEVICE_INFO"],
                ["read", "write", "notify"], service)

    """
    We expect value to be a comma separated string of characteristic names.
    For each name, update the value of the relevant characteristic (via updateDeviceInfo().
    """
    def WriteValue(self, value, options):
        output = bytes(value).decode()
        print(output)
        names = output.split(',')
        for token in names:
            updateDeviceInfo(token)

class WIFI_CREDS_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        Characteristic.__init__(
                self, uuids["WIFI_CREDS"],
                ["read", "write"], service)

    def WriteValue(self, value, options):
        output = bytes(value).decode()
        splitpos = int(uuids["WIFI_CREDS_MAX_LENGTH"])
        wifi_ap = str(output[0:splitpos]).strip()
        wifi_passwd = str(output[splitpos:]).strip()
        #print("AP = {}, passwd = {}".format(wifi_ap, wifi_passwd))
        os.system('nmcli dev wifi connect {} password {}'.format(wifi_ap, wifi_passwd))
        # Let client know the new SSID, IP
        wifi_ap = bleService.get_characteristic_by_uuid(uuids["INFO_WIFI_AP"])
        wifi_ap.set_wifi_ap_callback()
        wifi_ip = bleService.get_characteristic_by_uuid(uuids["INFO_WIFI_IP"])
        wifi_ip.set_wifi_ip_callback()

    def get_wifi_creds_length(self):
        value = []
        strtemp = uuids["WIFI_CREDS_MAX_LENGTH"]
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_wifi_creds_length_callback(self):
        if self.notifying:
            value = self.get_wifi_creds_length()
            self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_wifi_creds_length()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_wifi_creds_length_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_wifi_creds_length()

class CMD_LOCATE_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.blink = False
        Characteristic.__init__(
                self, uuids["CMD_LOCATE"],
                ["write"], service)

    def WriteValue(self, value, options):
        output = bytes(value).decode()
        if output == "toggle":
            self.blink = not self.blink
            if self.blink:
                os.system('echo heartbeat | sudo dd status=none of=/sys/class/leds/ACT/trigger')
            else:
                os.system('echo default-on | sudo dd status=none of=/sys/class/leds/ACT/trigger')

class CMD_REBOOT_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        Characteristic.__init__(
                self, uuids["CMD_REBOOT"],
                ["write"], service)

    def WriteValue(self, value, options):
        output = bytes(value).decode()
        print("CMD_REBOOT_Characteristic {}".format(output))
        if output == "reboot":
            print("rebooting")
            """
            No more REPORTS_ID
            reports = bleService.get_characteristic_by_uuid(uuids["REPORTS_ID"])
            reports.send_disconnect_message()
            """
            os.system('sudo reboot')

class CMD_RUNBLE_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        Characteristic.__init__(
                self, uuids["CMD_RUNBLE"],
                ["write"], service)

    def WriteValue(self, value, options):
        output = bytes(value).decode()
        print("CMD_RUNBLE_Characteristic {}".format(output))
        if output == "stop_ble":
            print("stopping BLE")
            """
            No more REPORTS_ID
            reports = bleService.get_characteristic_by_uuid(uuids["REPORTS_ID"])
            reports.send_disconnect_message()
            """
            app.quit()


class INFO_DEVICE_ID_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_DEVICE_ID"],
                ["read", "notify"], service)

    def get_device_id(self):
        value = []
        strtemp = BLE_DEVICE_NAME
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_device_id_callback(self):
        if self.notifying:
            value = self.get_device_id()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_device_id()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_device_id_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_device_id()

class INFO_MODEL_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_MODEL"],
                ["read", "notify"], service)

    def get_model(self):
        value = []
        with open('/proc/device-tree/model') as f:
            strtemp = f.read()
        for c in strtemp[:-1]:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_model_callback(self):
        if self.notifying:
            value = self.get_model()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_model()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_model_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_model()

class INFO_WIFI_AP_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_WIFI_AP"],
                ["read", "notify"], service)

    def get_wifi_ap(self):
        value = []
        try:
            strtemp = os.popen('iwgetid').read().split(":")[1].strip().strip('"')
        except:
            strtemp = "....."
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_wifi_ap_callback(self):
        if self.notifying:
            value = self.get_wifi_ap()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_wifi_ap()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_wifi_ap_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_wifi_ap()


class INFO_WIFI_IP_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_WIFI_IP"],
                ["read", "notify"], service)

    def get_wifi_ip(self):
        value = []
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)	
        s.connect(("8.9.10.11", 80))
        strtemp = s.getsockname()[0]
        s.close()
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_wifi_ip_callback(self):
        if self.notifying:
            value = self.get_wifi_ip()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_wifi_ip()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_wifi_ip_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_wifi_ip()

class INFO_CPU_TEMP_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_CPU_TEMP"],
                ["read", "notify"], service)

    def get_cpu_temp(self):
        # Celsius
        value = []
        sensor_file='/sys/class/thermal/thermal_zone0/temp'
        with open(sensor_file) as f:
           strtemp = "{:.2f} C".format(float(f.read().strip()) / 1000)
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_cpu_temp_callback(self):
        if self.notifying:
            value = self.get_cpu_temp()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_cpu_temp()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_cpu_temp_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_cpu_temp()

class INFO_CPU_LOAD_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_CPU_LOAD"],
                ["read", "notify"], service)

    def get_cpu_load(self):
        value = []
        #file_columns: 0 is 1min average, 1 is 5min, 2 is 15min
        with open('/proc/loadavg') as f:
           file_columns = f.read().strip().split()
           strtemp = "{:.2f} %".format(float(file_columns[LOAD_AVG_5]) * 100)
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_cpu_load_callback(self):
        if self.notifying:
            value = self.get_cpu_load()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_cpu_load()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_cpu_load_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_cpu_load()

class INFO_CPU_USAGE_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_CPU_USAGE"],
                ["read", "notify"], service)

    def get_cpu_usage(self):
        IDLE = 4
        with open('/proc/stat', 'r') as procfile:
            cputimes = procfile.readline().split()
            cputotal = 0
            for i in cputimes[1:-1]: # not guest_nice
                i = int(i)
                cputotal = (cputotal + i)
            cpu_usage = (100 - float(cputimes[IDLE])*100/float(cputotal))
        value = []
        strtemp = "{:.2f} %".format(cpu_usage)
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_cpu_usage_callback(self):
        if self.notifying:
            value = self.get_cpu_usage()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_cpu_usage()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_cpu_usage_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_cpu_usage()

class INFO_MEMORY_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_MEMORY"],
                ["read", "notify"], service)

    def getRAMinfo(self):
        p = os.popen('free')
        i = 0
        while 1:
            i = i + 1
            line = p.readline()
            if i == 2:
                return(line.split()[1:4]) 

    def get_memory(self):
        value = []
        ramInfo = self.getRAMinfo()	# [total, used, free]
        strtemp = "{}/{} Free".format(ramInfo[2],ramInfo[0])
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_memory_callback(self):
        if self.notifying:
            value = self.get_memory()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_memory()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_memory_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_memory()

class INFO_UPTIME_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_UPTIME"],
                ["read", "notify"], service)

    def get_uptime(self):
        value = []
        with open('/proc/uptime') as f:
           file_columns = f.read().strip().split()
           # Client expects microseconds
           strtemp = "{}".format(float(file_columns[0]) * 1000000)
        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_uptime_callback(self):
        if self.notifying:
            value = self.get_uptime()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_uptime()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_uptime_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_uptime()

class INFO_BATTERY_Characteristic(Characteristic):
    def __init__(self, service):
        self.notifying = False
        self.last_value = []
        Characteristic.__init__(
                self, uuids["INFO_BATTERY"],
                ["read", "notify"], service)

    def get_battery(self):
        value = []
        
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.connect(('localhost', 8423))
            message = 'get battery'
            sock.sendall(message.encode('utf8'))
            data = ''
            while not data.endswith("\n"):
                data += bytes(sock.recv(20)).decode()
        except:
            strtemp = '.....'
        else:
            strtemp = '{:.2f} %'.format(float(data.split()[1].strip()))
        finally:
            sock.close()

        for c in strtemp:
            value.append(dbus.Byte(c.encode()))
        return value

    def set_battery_callback(self):
        if self.notifying:
            value = self.get_battery()
            if value != self.last_value:
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
                self.last_value = value
        return self.notifying

    def StartNotify(self):
        if self.notifying:
            return
        self.notifying = True
        value = self.get_battery()
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":value}, [])
        self.add_timeout(NOTIFY_TIMEOUT, self.set_battery_callback)

    def StopNotify(self):
        self.notifying = False

    def ReadValue(self, options):
        return self.get_battery()


"""
For the given item,
find the relevant characteristic and update its value
"""
def updateDeviceInfo(item): 
    if item == "device_id":
        #print("updateDeviceInfo for device_id")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_DEVICE_ID"])
    elif item == "model":
        #print("updateDeviceInfo for model")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_MODEL"])
    elif item == "wifi_ap":
        #print("updateDeviceInfo for wifi_ap")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_WIFI_AP"])
    elif item == "wifi_ip":
        #print("updateDeviceInfo for wifi_ip")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_WIFI_IP"])
    elif item == "cpu_temp":
        #print("updateDeviceInfo for cpu_temp")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_CPU_TEMP"])
    elif item == "cpu_load":
        #print("updateDeviceInfo for cpu_load")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_CPU_LOAD"])
    elif item == "cpu_usage":
        #print("updateDeviceInfo for cpu_usage")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_CPU_USAGE"])
    elif item == "memory":
        #print("updateDeviceInfo for memory")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_MEMORY"])
    elif item == "uptime":
        #print("updateDeviceInfo for uptime")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_UPTIME"])
    elif item == "battery":
        #print("updateDeviceInfo for battery")
        characteristic = bleService.get_characteristic_by_uuid(uuids["INFO_BATTERY"])
    elif item == "cmd_locate":
        #print("updateDeviceInfo for cmd_locate")
        characteristic = bleService.get_characteristic_by_uuid(uuids["CMD_LOCATE"])
        characteristic.get_locate()
    else:
        print("updateDeviceInfo for unknown item {}".format(item))

app = Application()
bleService = BLEService(0)
app.add_service(bleService)
app.register()

adv = ConfiguratorAdvertisement(0)
adv.register()

try:
    app.run()
except KeyboardInterrupt:
    app.quit()


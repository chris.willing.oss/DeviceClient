## BLE Configuration for ESP32 devices

This sub project implements a BLE peripheral role application to run on ESP32 devices as a configuration service. It enables remote configuration of wireless SSID and password as well as monitoring of some run time characteristics.

The configuration service is implemented so as to require as little change as possible when writing a new sketch for the device. Therefore this configuration service sketch can be a template for any application to run on the device. An example of this methodology is available in the _MqttDeviceclient_ directory; it uses the configuration service sketch as a base over which the _MqttDeviceclient_  application is added.


### Prerequisites

- ESP32 device


### Installation

The configuration service may be installed via:
- Arduino IDE
- uploading a supplied image.

Both methods will erase any existing software on the device.

An advantage of loading with the Arduino IDE is that other software that the device is intended to run may then be added.

An advantage of uploading a premade image is that when setting up a number of devices, there's no need to to run Arduino IDE for each of them.


#### Install with Arduino IDE
- [Download the source tarball](https://gitlab.com/chris.willing.oss/DeviceClient/-/releases) and unpack it (tar xf DeviceClientBLE_Configure-0.0.5.tar.gz)
- In the Arduino IDE, generate a _New Sketch_ and _Open_ the _DeviceClientBLE_Configure.ino_ file in the _DeviceClientBLE_Configure_ directory which was created when unpacking the source tarball.

Notice that the sketch is mostly empty, containing only a _#include_ entry and a call to _setupConfigurator()_ in the _setup_ section. Normal application code for the device could now be added, however it is also an opportunity to first test the configuration system. In either case, select the _BleConfigure.cpp_ tab and, near the beginning of the file, edit the definition of LED pin number to suit the particular ESP32 board being used (since different boards use different pins). Compile and upload the sketch to the board.


### Usage

When the device is booted it will make itself accessible via BLE configuration service. A suitable client program is required to communicate with the ESP32 device over BLE. [A suitable client device is available here](https://gitlab.com/chris.willing.oss/deviceclientconfig).

![](pix/smConfigClientESP32.png)

Whether served from a web server or run from a file in a browser (as in the pix above), the browser must be Bluetooth capable e.g. Google's Chrome browser.

In the left _Device Information_ panel, various facets of the peripheral device are regularly updated.

Of the command buttons in the right _Device Commands_ panel, the purpose of the _REBOOT device_ button is obvious. The _Stop BLE_ button shuts down the configuration application (no further BLE communication is then possible). The _Locate device_ button toggles flashing of the device's onboard LED. This is useful for identifying which of possibly many devices is currently connected.

The middle _Wifi Setting_ panel enables a wifi SSID and password to be sent to the device. This is most useful if the configuration software is able to be included in the boot image initially burnt into the Pi Zero's SD. In this case, individual wifi access can be facilitated for multiple headless systems all using the same boot image.


### Support

Any problems should be addressed to the [DeviceClient Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) page.


### Contributing

Contributions are welcome either as [Merge Requests](https://gitlab.com/chris.willing.oss/DeviceClient/-/merge_requests) or comments to the [DeviceClient Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) page.


### License
MIT (http://opensource.org/licenses/MIT)

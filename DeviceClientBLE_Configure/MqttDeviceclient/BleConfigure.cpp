#include "BleConfigure.h"
#include "deviceClientBleUuids.h"

//  Boards have LED at different pin locations. 2 & 5 are common, even 17. Set as required,
//  OR comment out if no onboard LED
#define LED 2
//#define LED 5
//#define LED 17  // EZSBC boards 2nd LED green channel (LED_OFF is HIGH)
//  Some LED arrangements require LOW to be off, some require HIGH to be off. Set as required.
#define LED_OFF LOW
//#define LED_OFF HIGH

//  Comment out to enable battery information
#undef  INFO_BATTERY

TimerHandle_t deviceInfoUpdateTimer;
TimerHandle_t internalLedFlashTimer;
bool toggleLED = false;



// Timer variables
// 30000 = 30 seconds
//  3000 =  3 seconds
unsigned long timerDelay = 30000;
unsigned long lastTime = 0;

//  Bluetooth connection state
bool deviceConnected = false;
bool oldDeviceConnected = false;

Preferences preferences;

char chip_model[16];
char fullHostName[19];
int WiFiStatus;


NimBLEServer* pServer = NULL;

// Configuration Service
NimBLEService* configService = NULL;
NimBLECharacteristic* device_info = NULL;
NimBLECharacteristic* wifi_creds = NULL;
//NimBLECharacteristic* pCharacteristic_3 = NULL;  // Commands
NimBLECharacteristic* cmd_locate = NULL;  // Flash LED
NimBLECharacteristic* cmd_reboot = NULL;  // Reboot
NimBLECharacteristic* cmd_runble = NULL;  // Stop BLE
//  Device Indo characteristics
NimBLECharacteristic* info_device_id = NULL;
NimBLECharacteristic* info_model = NULL;
NimBLECharacteristic* info_wifi_ap = NULL;
NimBLECharacteristic* info_wifi_ip = NULL;
NimBLECharacteristic* info_cpu_temp = NULL;
NimBLECharacteristic* info_cpu_load = NULL;
NimBLECharacteristic* info_memory = NULL;
NimBLECharacteristic* info_uptime = NULL;
NimBLECharacteristic* info_battery = NULL;
NimBLECharacteristic* info_cpu_usage = NULL;


//Setup callbacks onConnect and onDisconnect
class MyServerCallbacks : public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    deviceConnected = true;
  };
  void onDisconnect(BLEServer* pServer) {
    deviceConnected = false;
    NimBLEDevice::startAdvertising();
  }
};

void updateDeviceInfo(char *item) {
  String infoItem = String(item);

  if (infoItem == "device_id") {
    //Serial.println(F("updateDeviceInfo() for device_id"));
    info_device_id->setValue(String(fullHostName));
    info_device_id->notify();
  }
  else if (infoItem == "model") {
    //Serial.println(F("updateDeviceInfo() for model"));
    info_model->setValue(String(chip_model));
    info_model->notify();
  }
  else if (infoItem == "wifi_ap") {
    //Serial.println(F("updateDeviceInfo() for wifi_ap"));
    info_wifi_ap->setValue(String(preferences.getString("ssid", "....")));
    info_wifi_ap->notify();
  }
  else if (infoItem == "wifi_ip") {
    //Serial.println(F("updateDeviceInfo() for wifi_ip"));
    info_wifi_ip->setValue(WiFi.localIP().toString());
    info_wifi_ip->notify();
  }
  else if (infoItem == "cpu_temp") {
    //Serial.println(F("updateDeviceInfo() for cpu_temp"));
    info_cpu_temp->setValue(String(temperatureRead()));
    info_cpu_temp->notify();
  }
  else if (infoItem == "cpu_load") {
    Serial.println(F("updateDeviceInfo() for cpu_load"));
    info_cpu_load->setValue(String("xxx"));
    info_cpu_load->notify();
  }
  else if (infoItem == "cpu_usage") {
    Serial.println(F("updateDeviceInfo() for cpu_usage"));
    info_cpu_usage->setValue(String("yyy"));
    info_cpu_usage->notify();
  }
  else if (infoItem == "memory") {
    //Serial.println(F("updateDeviceInfo() for memory"));
    char message[16];
    sprintf(message, "%s/%s\0", String(ESP.getFreeHeap()), String(ESP.getHeapSize()));
    //Serial.print(F("infoItem memory = ")); Serial.println(message);
    //Serial.print(F("infoItem memory length = ")); Serial.println(strlen(message));
    info_memory->setValue(String(message));
    info_memory->notify();
  }
  else if (infoItem == "uptime") {
    //Serial.println(F("updateDeviceInfo() for uptime"));
    char message[32];
    sprintf(message, "%lld\0", esp_timer_get_time());
    //Serial.print(F("infoItem uptime = ")); Serial.println(message);
    //Serial.print(F("infoItem uptime length = ")); Serial.println(strlen(message));
    info_uptime->setValue(String(message));
    info_uptime->notify();
  }
  else if (info_battery && (infoItem == "battery")) {
    //Serial.println(F("updateDeviceInfo() for battery"));
    char message[32];
    //  This is just to test throughput - nothing to do with battery state
    //  Insert appropriate code to determine battery state
    sprintf(message, "%lld\0", esp_timer_get_time());
    //Serial.print(F("infoItem battery = ")); Serial.println(message);
    //Serial.print(F("infoItem battery length = ")); Serial.println(strlen(message));
    info_battery->setValue(String(message));
    info_battery->notify();
  }
  else
    Serial.println(F("Unknown infoItem at updateDeviceInfo()"));

}

//  SSID & password, each padded to a length of 20 bytes,
//  arrive in a single string (of 40 bytes total).
//  Change the padded length of each by setting WIFI_CREDS_MAX_LENGTH
void setNewWifiCredentials(String message) {
  //Serial.print(F("Received message is: ")); Serial.println(message);
  const int text_length = atoi(WIFI_CREDS_MAX_LENGTH); 
  String ssid = message.substring(0, text_length); ssid.trim();
  String passwd = message.substring(text_length); passwd.trim();

  preferences.putString("ssid", ssid);
  preferences.putString("passwd", passwd);

  //  Reset the characteristic
  wifi_creds->setValue(String(WIFI_CREDS_MAX_LENGTH));
  wifi_creds->notify();
}


void deviceInfoUpdateCB (TimerHandle_t xTimer) {
  if (deviceConnected) {
    updateDeviceInfo("memory");
    updateDeviceInfo("cpu_temp");
    updateDeviceInfo("uptime");
    if (info_battery)
      updateDeviceInfo("battery");
  }
}
void setupDeviceInfoUpdateTimer () {
  /* 1000MS should be 1sec */
  deviceInfoUpdateTimer = xTimerCreate("deviceInfoUpdates", pdMS_TO_TICKS(1000), pdTRUE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(deviceInfoUpdateCB));
  xTimerStart(deviceInfoUpdateTimer, 0);
}

void flashLedCB(TimerHandle_t xTimer) {
#ifdef LED
  if (!toggleLED) {
    digitalWrite(LED, LED_OFF);
    return;
  }

  if (digitalRead(LED) == HIGH) {
    digitalWrite(LED, LOW);
  } else {
     digitalWrite(LED, HIGH);
  }
#endif
  return;
}
void setupLedFlashTimer() {
#ifdef LED
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LED_OFF);

  /* 1000MS should be 1sec */
  internalLedFlashTimer = xTimerCreate("internalLedFlasher", pdMS_TO_TICKS(250), pdTRUE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(flashLedCB));
  xTimerStart(internalLedFlashTimer, 0);
#endif
  return;
}

class CharacteristicCallbacks : public NimBLECharacteristicCallbacks {

  void onRead(NimBLECharacteristic* pCharacteristic) {
    Serial.print(": onRead BLE ");
    Serial.print(pCharacteristic->getUUID().toString().c_str());
    Serial.print(": onRead(), value: ");
    String remoteHeading = pCharacteristic->getValue().c_str();
    Serial.println(remoteHeading);
  };

  void onWrite(NimBLECharacteristic* pCharacteristic) {
    Serial.print(": onWrite ");
    Serial.print(pCharacteristic->getUUID().toString().c_str());
    Serial.print(": onWrite(), value: ");
    Serial.println(pCharacteristic->getValue().c_str());

    String uuid = String(pCharacteristic->getUUID().toString().c_str());
    if (uuid == DEVICE_INFO) {
      Serial.println(F("device info request"));
      //sendDeviceInfoItems();
      char names[64];
      strncpy(names, pCharacteristic->getValue().c_str(), 64);
      char *token = strtok(names, ",");
      while (token != NULL) {
        //Serial.println(token);
        updateDeviceInfo(token);
        token = strtok(NULL, ",");
      }
      pCharacteristic->setValue(String("clear"));

    } else if (uuid == WIFI_CREDS) {
      setNewWifiCredentials(String(pCharacteristic->getValue().c_str()));
      ESP.restart();

    } else if (uuid == CMD_LOCATE) {
      if (String(pCharacteristic->getValue().c_str()) == CMD_LOCATE_TOGGLE) {
        //Serial.println(F("toggling"));
        if (toggleLED)
          toggleLED = false;
        else
          toggleLED = true;
      }

    } else if (uuid == CMD_REBOOT) {
      if (String(pCharacteristic->getValue().c_str()) == "reboot") {
        //Serial.println(F("rebooting"));
        ESP.restart();
      }

    } else if (uuid == CMD_RUNBLE) {
      if (String(pCharacteristic->getValue().c_str()) == "stop_ble") {
        //Serial.println(F("Stopping BLE"));
        BLEDevice::deinit();
      }

    } else {
      Serial.println(F("Nah"));
      Serial.println(uuid);
    }
  };
};


void setupAdvertising() {
  BLEAdvertising* pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_ID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  NimBLEDevice::startAdvertising();
  //Serial.println(F("startAdvertising() - waiting for a client connection ..."));
}

void startBle() {
  BLEDevice::init("dummy");
  sprintf(fullHostName, "esp32-%06llx", BLEDevice::getAddress());
  BLEDevice::deinit();

  BLEDevice::init(fullHostName);

  pServer = NimBLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Configuration Service
  configService = pServer->createService(CONFIG_SERVICE);

  // Create a BLE Characteristic to request a device info report (in addition to normal notified reports)
  device_info = configService->createCharacteristic(DEVICE_INFO, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY);
  device_info->setCallbacks(new CharacteristicCallbacks());

  // Create a BLE Characteristic to send Wifi details
  wifi_creds = configService->createCharacteristic(WIFI_CREDS, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY);
  wifi_creds->setValue(String(WIFI_CREDS_MAX_LENGTH));
  wifi_creds->setCallbacks(new CharacteristicCallbacks());

  // Create a BLE Characteristic to receive custom commands - NOT USED
  //pCharacteristic_3 = configService->createCharacteristic(COMMANDS, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY);
  //pCharacteristic_3->setValue("nocommand");

  // Create individual BLE Characteristics for custom commands
#ifdef LED
  cmd_locate = configService->createCharacteristic(CMD_LOCATE, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY);
  cmd_locate->setCallbacks(new CharacteristicCallbacks());
#endif
  
  cmd_reboot = configService->createCharacteristic(CMD_REBOOT, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY);
  cmd_reboot->setCallbacks(new CharacteristicCallbacks());
  cmd_runble = configService->createCharacteristic(CMD_RUNBLE, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY);
  cmd_runble->setCallbacks(new CharacteristicCallbacks());

  // Create individual BLE Characteristics for device info items
  info_device_id = configService->createCharacteristic(INFO_DEVICE_ID, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
  info_model = configService->createCharacteristic(INFO_MODEL, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
  info_wifi_ap = configService->createCharacteristic(INFO_WIFI_AP, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
  info_wifi_ip = configService->createCharacteristic(INFO_WIFI_IP, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
  info_cpu_temp = configService->createCharacteristic(INFO_CPU_TEMP, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
#if 0 // Don't have measurements for these yet
  info_cpu_load = configService->createCharacteristic(INFO_CPU_LOAD, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
  info_cpu_usage = configService->createCharacteristic(INFO_CPU_USAGE, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
#endif
  info_memory = configService->createCharacteristic(INFO_MEMORY, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
  info_uptime = configService->createCharacteristic(INFO_UPTIME, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
#ifdef INFO_BATTERY
  info_battery = configService->createCharacteristic(INFO_BATTERY, NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
#endif

  configService->start();
  setupAdvertising();

  //  TODO: check whether we need to request same setting with any connecting clients
  if (NimBLEDevice::getMTU() < (uint16_t)BLE_MTU_PREF) {
    NimBLEDevice::setMTU((uint16_t)BLE_MTU_PREF);
  }
  Serial.print(F("BLE MTU set to ")); Serial.println(NimBLEDevice::getMTU());
}



String Get_WiFiStatus(int Status) {
  switch (Status) {
    case WL_IDLE_STATUS:
      return "WL_IDLE_STATUS";
    case WL_SCAN_COMPLETED:
      return "WL_SCAN_COMPLETED";
    case WL_NO_SSID_AVAIL:
      return "WL_NO_SSID_AVAIL";
    case WL_CONNECT_FAILED:
      return "WL_CONNECT_FAILED";
    case WL_CONNECTION_LOST:
      return "WL_CONNECTION_LOST";
    case WL_CONNECTED:
      return "WL_CONNECTED";
    case WL_DISCONNECTED:
      return "WL_DISCONNECTED";
  }
}

void startWifi() {
  Serial.println(F("Connecting wifi ..."));

  WiFi.begin(preferences.getString("ssid", ""), preferences.getString("passwd", ""));
  WiFiStatus = WiFi.status();

  int wifi_tries = 0;
  while (WiFiStatus != WL_CONNECTED && (wifi_tries < 40)) {
    delay(250);
    WiFiStatus = WiFi.status();
    Serial.println(Get_WiFiStatus(WiFiStatus));
    wifi_tries += 1;
  }
  if (WiFiStatus == WL_CONNECTED) {
    Serial.print(F("Connected to Wifi network with IP address: ")); Serial.println(WiFi.localIP());
  } else {
    Serial.print(F("No wifi connection. Try bluetooth Device Configurator for device: ")); Serial.println(fullHostName);
  }
}

void
setupConfigurator () {
  preferences.begin("wifi", false);  // true => read only

  setupLedFlashTimer();
  setupDeviceInfoUpdateTimer();

  sprintf(chip_model, "%s\0", ESP.getChipModel());

  startBle();
  startWifi();
}

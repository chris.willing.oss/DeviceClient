extern "C" {
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
}

#include <WiFi.h>
#include <Preferences.h>

#define CONFIG_BT_NIMBLE_ROLE_PERIPHERAL
#include <NimBLEDevice.h>

void setupConfigurator();


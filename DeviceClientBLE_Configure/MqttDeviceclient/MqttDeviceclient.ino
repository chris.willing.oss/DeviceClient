
#include "BleConfigure.h"

#include "AsyncUDP.h"
#include <AsyncMQTT_ESP32.h>

#include <ArduinoJson.h>
#include <ArduinoJson.hpp>

#include <Keypad.h>

#include "esp_adc_cal.h"

#define SUB_TOPIC "/dcdp/server/#"
#define PUB_TOPIC "/dcdp/node"
#define DISCOVER_INTERVAL (3000)
#define DISCOVERY_PORT (48895)


#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 900      /* Time for which ESP32 will go stay sleep (in seconds) */

RTC_DATA_ATTR AsyncUDP udp;


bool DCDP_connected = false;

uint16_t
choose_udp_port(uint64_t start) {
  uint16_t udp_port = random(start) % 65536;
  while (udp_port < 49152) {
    udp_port += (65536 - 49152);
  }
  return udp_port;
}

uint64_t macnum = ESP.getEfuseMac();
extern char fullHostName[];
RTC_DATA_ATTR uint16_t cport;  // = choose_udp_port(macnum);

void print_wakeup_reason() {
  esp_sleep_wakeup_cause_t wakeup_reason = esp_sleep_get_wakeup_cause();

  switch (wakeup_reason) {
    case ESP_SLEEP_WAKEUP_EXT0: Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1: Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER: Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD: Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP: Serial.println("Wakeup caused by ULP program"); break;
    default: Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason); break;
  }
}


const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the symbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {27, 14, 12, 13}; //connect to the column pinouts of the keypad
byte colPins[COLS] = {19, 21, 22, 23}; //connect to the row pinouts of the keypad
/*
const byte ROWS = 4;  //four rows
const byte COLS = 3;  //three columns
//define the symbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  { '1', '2', '3' },
  { '4', '5', '6' },
  { '7', '8', '9' },
  { '*', '0', '#' }
};
byte rowPins[ROWS] = { 27, 14, 12, 13 };  //connect to the row pinouts of the keypad
byte colPins[COLS] = { 19, 21, 22 };      //connect to the column pinouts of the keypad
*/

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);


struct ConfigData {
  char device[32];
  char client_name[32];
  char vendor_id[16];
  char product_id[16];
  char unit_id[16];
  char rowCount[8];
  char colCount[8];
  char svr_address[16];
  char svr_port[8];
  char caddress[16];
  char cport[8];
};
ConfigData* config = (ConfigData*)std::calloc(sizeof(ConfigData), 1);






AsyncMqttClient mqttClient;
TimerHandle_t mqttReconnectTimer;
TimerHandle_t wifiReconnectTimer;
TimerHandle_t dcActivityTimer;


void setup() {
  // Debug console
  Serial.begin(115200);
  while (!Serial)
    ;

  delay(200);

  // Start BLE configuration service
  setupConfigurator();

  print_wakeup_reason();
  /*  Wake from sleep leaves a valid cport in RTC memory which we use.
  *   A reboot requires a new cport value.
  */
  if ((cport < 49152) || (cport > 65535)) {
    cport = choose_udp_port(macnum);
    Serial.printf("Set new cport: %d\n", cport);
  }

  Serial.print(F("cport = ")); Serial.println(cport);


  Serial.print(F("Hostname: ")); Serial.println(fullHostName);

  Serial.println(readBattery());




  customKeypad.addEventListener(keypadEvent);  // Add an event listener for this keypad
  customKeypad.setHoldTime(10);                // Default is 1000mS

  // Device configuration
  strlcpy(config->device, "RemoteButtonTest", String("RemoteButtonTest").length() + 1);
  itoa(1523, config->vendor_id, 10);
  itoa(9167, config->product_id, 10);
  itoa(0, config->unit_id, 10);
  itoa(ROWS, config->rowCount, 10);
  itoa(COLS, config->colCount, 10);
  config->svr_address[0] = '\0';
  config->svr_port[0] = '\0';
  IPAddress ip = WiFi.localIP();
  sprintf(config->caddress, "%d.%d.%d.%d\0", ip[0], ip[1], ip[2], ip[3]);
  itoa(cport, config->cport, 10);

  mqttReconnectTimer = xTimerCreate("mqttTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0,
                                    reinterpret_cast<TimerCallbackFunction_t>(connectToMqtt));

  /* 900000MS should be 15mins */
  dcActivityTimer = xTimerCreate("activityTimer", pdMS_TO_TICKS(900000), pdTRUE, (void*)0,
                                 reinterpret_cast<TimerCallbackFunction_t>(startSnooze));
  xTimerStart(dcActivityTimer, 0);


  /* Set MQTT callbacks */
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onMessage(onMqttMessage);


  /* Set up reactions to network inputs */
  process_network();
  discoverDCDPserver();
}


void startSnooze() {
  Serial.println("Time to snooze ...");
  //esp_deep_sleep_start();
}

void loop() {

  char customKey = customKeypad.getKey();

}

void sendKeyEventMessage(int buttonID, int updown) {
  int keypressDocCapacity = 2 * JSON_OBJECT_SIZE(10);
  char output[2 * JSON_OBJECT_SIZE(10)] = { 0 };
  DynamicJsonDocument keypressDoc(keypressDocCapacity);

  keypressDoc["msg_type"] = "device_data";
  keypressDoc["event_type"] = "button_event";
  keypressDoc["control_id"] = buttonID;
  keypressDoc["row"] = (buttonID - 1) % ROWS;
  keypressDoc["col"] = (buttonID - 1) / ROWS;
  keypressDoc["value"] = updown;
  keypressDoc["timestamp"] = millis();
  keypressDoc["caddress"] = config->caddress;
  keypressDoc["cport"] = config->cport;

  serializeJson(keypressDoc, output);
  Serial.print("Sending keypressDoc: ");
  Serial.println(output);
  //Serial.print("keypressDoc size = "); Serial.println(strlen(output));

  mqttClient.publish(PUB_TOPIC, 0, true, output);
  xTimerReset(dcActivityTimer, 10);
}

// Keypad events.
void keypadEvent(KeypadEvent key) {
  switch (customKeypad.getState()) {
    case PRESSED:
      sendKeyEventMessage(index_by_character(key), 1);
      //Serial.println(index_by_character(key), 1);
      break;

    case RELEASED:
      sendKeyEventMessage(index_by_character(key), 0);
      //Serial.println(index_by_character(key), 0);
      break;

    case HOLD:
      //Serial.println("key HOLD");
      break;
  }

}

int index_by_character(char c) {
  int result;

  switch (c) {
    case '1':
      result = 1;
      break;
    case '4':
      result = 2;
      break;
    case '7':
      result = 3;
      break;
    case '*':
      result = 4;
      break;
    case '2':
      result = 5;
      break;
    case '5':
      result = 6;
      break;
    case '8':
      result = 7;
      break;
    case '0':
      result = 8;
      break;
    case '3':
      result = 9;
      break;
    case '6':
      result = 10;
      break;
    case '9':
      result = 11;
      break;
    case '#':
      result = 12;
      break;
    case 'A':
      result = 13;
      break;
    case 'B':
      result = 14;
      break;
    case 'C':
      result = 15;
      break;
    case 'D':
      result = 16;
      break;
    default:
      result = 0;
      break;
  }
  return result;
}

void discoverDCDPserver() {
  long lastRequest = 0;

  while (String(config->svr_address).length() < 1) {
    if (millis() - lastRequest < DISCOVER_INTERVAL) {
      continue;
    }
    Serial.println("discoverDCDPserver()");

    char output[64];
    DynamicJsonDocument discoverDoc(32);
    discoverDoc["msg_type"] = "discover";
    serializeJson(discoverDoc, output);
    udp.broadcastTo(output, DISCOVERY_PORT);

    lastRequest = millis();
    //Timer0Count = 0;
  }
}

void process_network() {
  if (udp.listen(DISCOVERY_PORT)) {
    Serial.println("Listening ...");
    udp.onPacket([](AsyncUDPPacket message) {
      DynamicJsonDocument msg(2 * message.length());
      DeserializationError error = deserializeJson(msg, message);
      if (error) {
        Serial.print("deserializeJson() failed: ");
        Serial.println(error.c_str());
      }
      String msg_type = msg["msg_type"].as<const char*>();
      if (msg_type == "discover_result") {
        //Serial.println("local port = " + String(message.localPort()));
        //Serial.println("remote port = " + String(message.remotePort()));
        /*  The server advertises an array of available services.
        *   Each service is described by an object containing
        *   fields of port, ifaddress and type
        */
        JsonArray services = msg["services"].as<JsonArray>();
        for (JsonVariant value : services) {
          JsonObject service = value.as<JsonObject>();
          //  We're only interested in MQTT service here (TCP and/or UDP may also be available)
          if (service["type"] == "mqtt") {
            // We may have several options to choose from.
            // Just take the first available & ignore others
            if (strlen(config->svr_port) > 0) {
              //Serial.println("Already have a port");
              //  i.e. ignore it
            } else {
              //Serial.println("no port yet");
              // Add address & port to configuration
              itoa(service["port"].as<unsigned short>(), config->svr_port, 10);
              strlcpy(config->svr_address, service["ifaddress"].as<const char*>(), String(service["ifaddress"].as<const char*>()).length() + 1);

              // Also add them to rtc memory (for wakeup from sleep)
              //strcpy(rtc_svr_address, config->svr_address);
              //strcpy(rtc_svr_port, config->svr_port);

              //device_connect();
              Serial.print("Using MQTT service at: ");
              Serial.println(config->svr_address);
              mqttClient.setClientId(fullHostName);
              mqttClient.setServer(config->svr_address, (uint16_t)atoi(config->svr_port));
              connectToMqtt();
            }
          }
        }
      } else {
        Serial.println("Unhandled msg_type: " + msg_type);
        Serial.print("From: ");
        Serial.println(message.remoteIP());
        Serial.write(message.data(), message.length());
        Serial.println();
      }
    });
  }
}

void printSeparationLine() {
  Serial.println("************************************************");
}

void onMqttConnect(bool sessionPresent) {
  Serial.print("Connected to MQTT broker: ");
  Serial.print(config->svr_address);
  Serial.print(", port: ");
  Serial.println(config->svr_port);
  Serial.print("PUB_TOPIC: ");
  Serial.println(PUB_TOPIC);

  printSeparationLine();
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  uint16_t packetIdSub = mqttClient.subscribe(SUB_TOPIC, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub);

  printSeparationLine();
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
  //Serial.println("ClientId 2 = " + String(mqttClient.getClientId()));
}

void onMqttSubscribe(const uint16_t& packetId, const uint8_t& qos) {
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);

  // Send connection request to DCDP server
  sendConnectMessage();
  xTimerReset(dcActivityTimer, 10);
}


void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  (void)reason;

  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    xTimerStart(mqttReconnectTimer, 0);
  }
}

void sendConnectMessage() {
  // Connect to DCDP server
  int connectDocCapacity = 2 * JSON_OBJECT_SIZE(11);
  char output[2 * JSON_OBJECT_SIZE(11)] = { 0 };
  DynamicJsonDocument connectDoc(connectDocCapacity);

  connectDoc["msg_type"] = "device_connect";
  connectDoc["device"] = config->device;
  connectDoc["client_name"] = fullHostName;
  connectDoc["vendor_id"] = config->vendor_id;
  connectDoc["product_id"] = config->product_id;
  connectDoc["unit_id"] = config->unit_id;
  connectDoc["rowCount"] = config->rowCount;
  connectDoc["colCount"] = config->colCount;
  connectDoc["caddress"] = config->caddress;
  connectDoc["cport"] = config->cport;

  serializeJson(connectDoc, output);
  Serial.print("Sending connectDoc: ");
  Serial.println(output);
  //Serial.print("connectDoc size = "); Serial.println(strlen(output));

  mqttClient.publish(PUB_TOPIC, 0, true, output);
}

/*  Reconstitute chunks into single message ready to deserialize.
*/
void onMqttMessage(char* topic, char* payload, const AsyncMqttClientMessageProperties& properties,
                   const size_t& len, const size_t& index, const size_t& total) {
  (void)payload;

  static char* payload_str = NULL;

  /*
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);
  */

  if (index == 0) {
    // New message
    payload_str = (char*)calloc(total + 1, sizeof(char));
  }
  memcpy(payload_str + index, payload, len);

  if ((index + len) >= total) {
    // Now have full message
    *(payload_str + total) = 0;

    /*  Don't try to deserialize messages which are too big
    if (total < 12999) {
      deserializeMessage(payload_str, total);
    }
    else {
      Serial.print("Not deserializing big message: "); Serial.println(String(payload_str));
      free(payload_str);
    }
    */
    deserializeMessage(payload_str, total);
  }
  //Serial.println(ESP.getFreeHeap()); Serial.println(ESP.getMinFreeHeap()); Serial.println(ESP.getHeapSize()); Serial.println(ESP.getMaxAllocHeap());Serial.println("******");
}

void deserializeMessage(char* payload, int total) {
  //Serial.print("deserializeMessage() received: "); Serial.println(String(payload));
  DynamicJsonDocument msg(3 * total);
  DeserializationError error = deserializeJson(msg, payload);
  if (error) {
    Serial.print("deserializeJson() failed: ");
    Serial.println(error.c_str());
  } else {
    String msg_type = msg["msg_type"].as<const char*>();
    if (msg_type == "device_connect_result") {
      if (msg["client_port"] == config->cport) {
        DCDP_connected = true;
        Serial.println("device_connect_result received OK");
      }
    } else if (msg_type == "disconnect_warning") {
      // Ensure it's really for us
      if (msg["client_port"] == config->cport) {
        sendConnectMessage();
      } else {
        Serial.println("Ignoring disconnect_warning for foreign client");
      }
    } else if (msg_type == "list_attached_result") {
      // This is emitted when a DCDP server is (re)starting.
      sendConnectMessage();
    } else if (msg_type == "button_event") { /* Ignore button_event */
    } else if (msg_type == "error") {
      String error_msg = msg["error_msg"].as<const char*>();
      if (error_msg.startsWith("Client not connected")) {
        // We could/should check if this error is for us or some other client
        // Even if not strictly for us, no harm in connecting again
        sendConnectMessage();
      } else {
        Serial.print("No handling error_msg: ");
        Serial.println(error_msg);
      }
    } else {
      Serial.print("Ignoring msg_type: ");
      Serial.println(msg_type);
    }
  }

  free(payload);
}

void
sendBatteryReport() {
  int reportDocCapacity = 2 * JSON_OBJECT_SIZE(10);
  char output[2 * JSON_OBJECT_SIZE(10)] = { 0 };
  DynamicJsonDocument reportDoc(reportDocCapacity);

  reportDoc["msg_type"] = "device_report";
  reportDoc["report_type"] = "battery";
  reportDoc["value"] = String(readBattery());
  reportDoc["timestamp"] = millis();
  reportDoc["caddress"] = config->caddress;
  reportDoc["cport"] = config->cport;

  serializeJson(reportDoc, output);
  Serial.print("Sending reportDoc: ");
  Serial.println(output);
  //Serial.print("keypressDoc size = "); Serial.println(strlen(output));

  mqttClient.publish(PUB_TOPIC, 0, true, output);
}

/******************************************************************************
        From: https://github.com/Torxgewinde/Firebeetle-2-ESP32-E/
Description.: reads the battery voltage through the voltage divider at GPIO34
              if the ESP32-E has calibration eFused those will be used.
              In comparison with a regular voltmeter the values of ESP32 and
              multimeter differ only about 0.05V
Input Value.: -
Return Value: battery voltage in volts
******************************************************************************/
float readBattery() {
  uint32_t value = 0;
  int rounds = 11;
  esp_adc_cal_characteristics_t adc_chars;

  //battery voltage divided by 2 can be measured at GPIO34, which equals ADC1_CHANNEL6
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
  switch(esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars)) {
    case ESP_ADC_CAL_VAL_EFUSE_TP:
      Serial.println("Characterized using Two Point Value");
      break;
    case ESP_ADC_CAL_VAL_EFUSE_VREF:
      Serial.printf("Characterized using eFuse Vref (%d mV)\r\n", adc_chars.vref);
      break;
    default:
      Serial.printf("Characterized using Default Vref (%d mV)\r\n", 1100);
  }

  //to avoid noise, sample the pin several times and average the result
  for(int i=1; i<=rounds; i++) {
    value += adc1_get_raw(ADC1_CHANNEL_6);
  }
  value /= (uint32_t)rounds;

  //due to the voltage divider (1M+1M) values must be multiplied by 2
  //and convert mV to V
  return esp_adc_cal_raw_to_voltage(value, &adc_chars)*2.0/1000.0;
}

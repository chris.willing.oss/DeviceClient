## MQTT Device Client application

This example project demonstrates how an application may be built on top of the [BLE based configuration service](https://gitlab.com/chris.willing.oss/DeviceClient/-/tree/main/DeviceClientBLE_Configure). In doing so, the application has a built in configuration service, enabling network SSID and password to be (re)set whenever necessary e.g. a new installation, moving location.


### Installation

- [Download the source tarball](https://gitlab.com/chris.willing.oss/DeviceClient/-/releases) and unpack it somewhere (`tar xf DeviceClientBLE_Configure-0.0.5.tar.gz`)
- With the Arduino IDE, generate a New Sketch and Open the _MqttDeviceClient.ino_ file in the _DeviceClientBLE_Configure/MqttDeviceClient_ directory which was created when unpacking the source tarball.
- Compile and upload the sketch to the device.


### Usage

Set the network SSID and password for the device with the [DeviceClientConfig](https://gitlab.com/chris.willing.oss/deviceclientconfig/) browser application. With correct network credentials now in place, the application searches for a DCDP server which supplies the application with the address of the MQTT server to be used. The application then sends data as required (in this case, keypress events) via MQTT to the DCDP server.


### Support

Please report any problems through the [DeviceClient Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) page.


### Contributing

Contributions are welcome either as [Merge Requests](https://gitlab.com/chris.willing.oss/DeviceClient/-/merge_requests) or comments to the [DeviceClient Issues](https://gitlab.com/chris.willing.oss/DeviceClient/-/issues) page.


### License

MIT (http://opensource.org/licenses/MIT)


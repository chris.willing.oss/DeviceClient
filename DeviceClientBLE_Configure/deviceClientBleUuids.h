#ifndef DEVICE_CLIENT_UUIDS_H
#define DEVICE_CLIENT_UUIDS_H

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_ID "9f9c635f-3321-4490-9bae-c58572125000"   // Advertised UUID (has no characteristics of its own)

#define CONFIG_SERVICE "9f9c635f-3321-4490-9bae-c58572125001"   // Configuration Service
#define DEVICE_INFO "9f9c635f-3321-4490-9bae-c58572125002"  // Specific request for device info
#define WIFI_CREDS "9f9c635f-3321-4490-9bae-c58572125003"   // Provide ssid,passwd,key
#define COMMANDS "9f9c635f-3321-4490-9bae-c58572125004"     // Send commands to the device
#define CMD_LOCATE "9f9c635f-3321-4490-9bae-c58572125005"   // Handle the locate function (flash LED)
#define CMD_REBOOT "9f9c635f-3321-4490-9bae-c58572125006"   // Handle reboot of device
#define CMD_RUNBLE "9f9c635f-3321-4490-9bae-c58572125007"   // Whether to stop device BLE

//  We choose which of these we make known
#define INFO_DEVICE_ID "9f9c635f-3321-4490-9bae-c58572125008" // Device ID
#define INFO_MODEL "9f9c635f-3321-4490-9bae-c58572125009"     // Device Model
#define INFO_WIFI_AP "9f9c635f-3321-4490-9bae-c58572125010"   // SSID to which device is attached
#define INFO_WIFI_IP "9f9c635f-3321-4490-9bae-c58572125011"   // IP address to which device is connected
#define INFO_CPU_TEMP "9f9c635f-3321-4490-9bae-c58572125012"  // Device internal temperature
#define INFO_CPU_LOAD "9f9c635f-3321-4490-9bae-c58572125013"  // Device CPU load
#define INFO_MEMORY "9f9c635f-3321-4490-9bae-c58572125014"    // Device memory usage
#define INFO_UPTIME "9f9c635f-3321-4490-9bae-c58572125015"    // Device up time
#define INFO_BATTERY "9f9c635f-3321-4490-9bae-c58572125016"   // Battery state
#define INFO_CPU_USAGE "9f9c635f-3321-4490-9bae-c58572125017" // Device CPU usage

#define DCDP_MSG_SVC "9f9c635f-3321-4490-9bae-c58572125100"   // DCDP Message service
#define DCDP_DEVICE_MSG "9f9c635f-3321-4490-9bae-c58572125101"// Characteristic for messages FROM the device
#define DCDP_SERVER_MSG "9f9c635f-3321-4490-9bae-c58572125102"// Characteristic for messages TO the device

#define WIFI_CREDS_MAX_LENGTH "20"

#define CMD_LOCATE_UNAVAILABLE "unavailable"  // Can't flash LED
#define CMD_LOCATE_AVAILABLE "available"      // Can flash LED
#define CMD_LOCATE_TOGGLE "toggle"            // Toggle flashing

#define BLE_MTU_PREF  (512)


#endif  // DEVICE_CLIENT_UUIDS_H
